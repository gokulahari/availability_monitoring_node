import React from 'react';
import "./App";
// import Login from './pages/login';
import Home from './pages/home';
class App extends React.Component {
    render() {
      return (
        <div className="App">
            <Home />
        </div>
      );
    }
}

export default App;