import React from 'react';
// import { Router, Route, Link, browserHistory, IndexRoute } from 'react-router';
import Header from '../components/header';
// import Home from './home';
import { loginApi } from '../server/loginApi';
import '../styles/css/login.css';

class Login extends React.Component {
    
    loginAuth(props) {
        const login = loginApi();
        login.then(res => {
            if (res === true) {
                console.log('true')
            }
            else {
                console.log('false')
            }
        });
    }

    render() {
        return (
            <div>
                <Header />
                <div className='container'>

                    <input type="text" placeholder="Enter Username" name="uname" required />

                    <label><b>Password</b></label>
                    <input type="password" placeholder="Enter Password" name="psw" required />

                    <button className='login-btn' onClick={() => { this.loginAuth() }} type="submit">Login</button>
                    <label>
                        <input type="checkbox" name="remember" /> Remember me
                    </label>
                </div>

                <div className="container">
                    <button type="button" className="cancelbtn">Cancel</button>
                    <span className="password"> <a href="#">Forgot password?</a></span>
                </div>
            </div>
        );
    }
}

export default Login;