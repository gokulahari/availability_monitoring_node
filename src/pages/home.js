import React from 'react';
import { Card, InputGroup, FormControl, Button } from 'react-bootstrap';
import Header from '../components/header';
import { urlApi } from '../server/urlApi';

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            val: "",
            duration: "",
            urlsList: [],
        };
    }

    componentDidMount() {
        this.fetchData();
    }

    fetchData() {
        try {
            fetch('https://availability-monitoring-node.herokuapp.com/home', {
                method: 'get',
                mode: 'cors',
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                }
            })
                .then(response => response.json())
                .then(res => {
                    if (res && res.data) {
                        console.log('response data....', res.data)
                        this.setState({ urlsList: [...this.state.urlsList, ...res.data] })
                    }
                })
        }
        catch (err) {
            console.log("this one.get..", err)
        }
    }


    async handleChange() {
        const urlapi = await urlApi(this.state.val, this.state.duration);
        console.log("ans...", urlapi);
        if (urlapi) {
            this.setState({ urlsList: [...this.state.urlsList, ...[urlapi]] })
        }
    }

    renderUrls() {
        if (this.state.urlsList.length <= 0) {
            return <div>Loading</div>
        }
        return this.state.urlsList.map((val, key) => {

            return <div key={key} style={{ padding: '20px' }} >
                <Card style={{ padding: '50px' }}>
                    <div className="row w-100 mx-auto flex justify-content-around">
                        <div className="col-xs-4 px-3">
                            {val.url}
                        </div>
                        <div className="col-xs-2 px-3">
                            {val.urlTime}
                        </div>
                        <div className="col-xs-2 px-3">
                            {val.urlStatus}
                        </div>
                        <div className="col-xs-2 px-3">
                            <img style={{ width: '30px', height: '30px' }} src={val.urlImage} alt="" />
                        </div>
                        <div className="col-xs-2 px-3">
                            {val.responseMessage}
                        </div>
                    </div>
                </Card>
            </div>;
        })
    }

    render() {
        return (
            <div>
                <Header />
                <div>
                    <Card style={{ padding: '100px' }}>
                        {/* <label htmlFor="basic-url">Enter the url</label> */}
                        <div className="row w-50 mx-auto flex">
                            <div className="col-xs-6 px-3">
                                <InputGroup className="mb-3">
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>Url</InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <FormControl style={{ width: '300px' }} value={this.state.val} onChange={e => this.setState({ val: e.target.value })} id="basic-url" aria-describedby="basic-addon3" />
                                </InputGroup>
                            </div>
                            <div className="col-xs-2 px-3">
                                <InputGroup className="mb-3">
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>Duration</InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <FormControl style={{ width: '100px' }} value={this.state.duration} onChange={e => this.setState({ duration: e.target.value })} id="basic-url" aria-describedby="basic-addon3" />
                                    <InputGroup.Append>
                                        <InputGroup.Text>ms</InputGroup.Text>
                                    </InputGroup.Append>
                                </InputGroup>
                            </div>
                            <div className="col-xs-4 px-3">
                                <Button onClick={() => this.handleChange()} variant="outline-success">Success</Button>{' '}
                            </div>
                        </div>
                    </Card>
                </div>
                <div >
                    {this.renderUrls()}
                </div>
            </div>
        );
    }
}

export default Home;