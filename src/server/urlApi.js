const urlApi = async function (url, duration) {
    console.log("urlApi...", url + "..." + duration)
    try {
        const res = await fetch('https://availability-monitoring-node.herokuapp.com/home', {
            method: 'post',
            mode: 'cors',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                url: url,
                duration: duration
            })
        }).then(response => response.json())
        if (res && res.urlDetail) {
            console.log("url details", res.urlDetail);
            return res.urlDetail;
        }
        else {
            console.log("no response coming");
        }
    }
    catch (err) {
        console.log("this one...", err)
    }
}

export { urlApi };