import history from './history'
const loginApi = async function() {
    let auth = false;
    try {
        await fetch('https://availability-monitoring-node.herokuapp.com/login', {
            method: 'post',
            mode: 'cors',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                email: 'customer@gmail.com',
                password: '123'
            })
        }).then(response => response.json())
            .then(res => {
                if (res && res.userSession) {
                    console.log("login details", res.userSession);
                    auth = true;
                    history.push("../pages/home");
                }
                else {
                    console.log("no response coming");
                }
            })
    }
    catch (err) {
        console.log("this one...", err)
    }
    return auth;
}

export { loginApi };